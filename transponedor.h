/*
 * transponedor.h
 *
 *  Created on: 12 Apr 2016
 *      Author: daniel
 */

#ifndef TRANSPONEDOR_H_
#define TRANSPONEDOR_H_

#include <systemc.h>

#include "fifo.h"

SC_MODULE (transponedor) {

public:
	sc_port<read_if_T<sc_int<16>>> dataIn[4]; // Recibe de iDCT
	sc_port<write_if_T<sc_int<16>>> hOut[4]; // Envía al Componedor
	sc_port<write_if_T<sc_int<16>>> vOut[4]; // Envía al iDCT

	void transponer();

	SC_CTOR (transponedor)
	{
		cout << "serieParalelo4: " << name() << endl;

		idIn = idOut = 0;

		SC_THREAD(transponer);
	}

private:
	sc_int <16> buffer[4][4];
	sc_uint <2> idIn, idOut;
};

#endif /* TRANSPONEDOR_H_ */
