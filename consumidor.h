/*
 * consumidor.h
 *
 *  Created on: 12 Apr 2016
 *      Author: daniel
 */

#ifndef CONSUMIDOR_H_
#define CONSUMIDOR_H_

#include <systemc.h>

#include <stdlib.h>
#include <stdio.h>

#include "fifo.h"

SC_MODULE (consumidor) {

	sc_in<sc_int<16>> in[4];
	sc_in<bool> valIn;
	sc_in<bool> reset;
	sc_in_clk clock;

public:
	SC_HAS_PROCESS(consumidor);

	void consumir();

	consumidor(sc_module_name name_, const char *fileName) : sc_module(name_)
	{
		cout << "consumidor: " << name() << "  " << fileName << endl;

		fichero = fopen(fileName, "rt");

		if (!fichero) {
			cerr << "No se puede abrir el fichero de entrada: " << fileName << endl;

			return;
		}

		SC_METHOD(consumir);
			sensitive << clock.pos();
	}

private:
	FILE *fichero = NULL;
	int i;
};

#endif /* CONSUMIDOR_H_ */
