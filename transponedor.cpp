#include "transponedor.h"

void transponedor::transponer()
{
	while (true) {
		dataIn[0]->read(buffer[idIn][0]);
		dataIn[1]->read(buffer[idIn][1]);
		dataIn[2]->read(buffer[idIn][2]);
		dataIn[3]->read(buffer[idIn][3]);

		wait(SC_ZERO_TIME);

		if (! ++idIn) {
			do {
				vOut[0]->write(buffer[0][idOut]);
				vOut[1]->write(buffer[1][idOut]);
				vOut[2]->write(buffer[2][idOut]);
				vOut[3]->write(buffer[3][idOut]);

				wait(SC_ZERO_TIME);

				dataIn[0]->read(buffer[0][idOut]);
				dataIn[1]->read(buffer[1][idOut]);
				dataIn[2]->read(buffer[2][idOut]);
				dataIn[3]->read(buffer[3][idOut]);

				wait(SC_ZERO_TIME);
			} while (++idOut);

			do {
				hOut[0]->write(buffer[idOut][0]);
				hOut[1]->write(buffer[idOut][1]);
				hOut[2]->write(buffer[idOut][2]);
				hOut[3]->write(buffer[idOut][3]);

				wait(SC_ZERO_TIME);
			} while (++idOut);
		}
	}
}
