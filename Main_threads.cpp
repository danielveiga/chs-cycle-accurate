#include <systemc>

#include <stdlib.h>

#include "fifo.h"
#include "productor.h"
#include "consumidor.h"
#include "transponedor.h"

char *nombreIdx(const char *cad, int idx)
{
	static char nombreYnumero[128];

	sprintf(nombreYnumero, "%s_%d", cad, idx);

	return nombreYnumero;
}

class top : public sc_module {

public:
	fifo_T<sc_int<16>> *qDatosIDCTProd[4]; // Datos que produce IDCT
	fifo_T<sc_int<16>> *qDatosIDCTCons[4]; // Datos que consume IDCT. Feedback de transponedor.
	fifo_T<sc_int<16>> *qDatosComponedorCons[4]; // Datos que consume el componedor. Producidos por transponedor.

	productor *entradaIDCT;
	consumidor *salidaComponedor, *salidaIDCT;
	transponedor *instTransponedor;

	SC_CTOR(top) {
		/*
		 * Instancia módulos.
		 */
		entradaIDCT = new productor("entradaIDCT", "transp_in.txt"); // IDCT como productor.
		salidaComponedor = new consumidor("salidaComponedor", "transp_out.txt"); // Componedor como consumidor.
		salidaIDCT = new consumidor("salidaIDCT", "transp_realim.txt"); // IDCT como consumidor.
		instTransponedor = new transponedor("instTransponedor"); // Transponedor.

		/*
		 * Incializa las colas.
		 */
		for (int i = 0; i < 4; i++) {
			qDatosIDCTProd[i] = new fifo_T<sc_int<16>> (nombreIdx("qDatosIDCTProd", i), 1);
			qDatosIDCTCons[i] = new fifo_T<sc_int<16>> (nombreIdx("qDatosIDCTCons", i), 1);
			qDatosComponedorCons[i] = new fifo_T<sc_int<16>> (nombreIdx("qDatosComponedorCons", i), 1);
		}

		/*
		 * Conecta las colas.
		 */
		for (int i = 0; i < 4; i++) {
			entradaIDCT->datos[i](*qDatosIDCTProd[i]); // IDCT produce en cola.
			instTransponedor->dataIn[i](*qDatosIDCTProd[i]); // Transponedor consume del IDCT
			instTransponedor->hOut[i](*qDatosComponedorCons[i]); // Transponedor produce para componedor
			instTransponedor->vOut[i](*qDatosIDCTCons[i]); // Transponedor produce para IDCT
			salidaComponedor->datos[i](*qDatosComponedorCons[i]); // Componedor consume de transponedor
			salidaIDCT->datos[i](*qDatosIDCTCons[i]); // IDCT consume de transponedor.
		}
	}
};

int sc_main(int nargs, char *vargs[])
{
	top principal("top");

	sc_start();

	return EXIT_SUCCESS;
}
