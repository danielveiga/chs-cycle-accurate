/*
 * productor.h
 *
 *  Created on: 12 Apr 2016
 *      Author: daniel
 */

#ifndef PRODUCTOR_H_
#define PRODUCTOR_H_

#include <systemc.h>

#include <stdlib.h>
#include <stdio.h>

#include "fifo.h"

SC_MODULE (productor) {

	sc_out<bool>				valOut;
	sc_out<sc_int<16>>			a0, a1, a2, a3;
	sc_in<bool>					reset;
	sc_in_clk					clock;

public:

	SC_HAS_PROCESS (productor);

	void producir();

	productor(sc_module_name name_, const char *fileName) : sc_module(name_)
	{
		cout << "productor: " << name() << "  " << fileName << endl;

		fichero = fopen(fileName, "rt");

		if (!fichero) {
			cerr << "No se puede abrir el fichero de entrada: " << fileName << endl;

			return;
		}

		SC_METHOD(producir);
			sensitive << clock.pos();
	}

private:
	FILE *fichero = NULL;
	int i, j;
};

#endif /* PRODUCTOR_H_ */
