#include "productor.h"

void productor::producir()
{
	int aa0, aa1, aa2, aa3;

	if (reset->read()) {
		valOut->write(false);
		i = 0;
	}

	if (i < 200) {
		fscanf(fichero, "%d", &aa0, &aa1, &aa2, &aa3);

		a0->write(aa0);
		a1->write(aa1);
		a2->write(aa2);
		a3->write(aa3);
		valOut->write(true);
	}else{
		fclose(fichero);
		valOut->write(false);
	}
}
