#include "consumidor.h"

void consumidor::consumir()
{
	sc_int<16> in0, in1, in2, in3;
	int fic0, fic1, fic2, fic3;

	if (reset->read()) {
		i = 0;
		return;
	}

	if (i < 200) {
		if (valIn->read()) {
			in0 = in[0]->read();
			in1 = in[1]->read();
			in2 = in[2]->read();
			in3 = in[3]->read();
			fscanf(fichero, "%d", &fic0, &fic1, &fic2, &fic3);
			if (fic0 != in0)
				cout << "Error @ " << i << " " << "0       " << fic0 << " != " << in0 << endl;
			if (fic1 != in1)
				cout << "Error @ " << i << " " << "1       " << fic0 << " != " << in0 << endl;
			if (fic2 != in2)
				cout << "Error @ " << i << " " << "2       " << fic0 << " != " << in0 << endl;
			if (fic3 != in3)
				cout << "Error @ " << i << " " << "3       " << fic0 << " != " << in0 << endl;
			i++;
		}
	}else{
		fclose(fichero);
		cout << "Finalizado" << endl;
		sc_stop();
	}
}
